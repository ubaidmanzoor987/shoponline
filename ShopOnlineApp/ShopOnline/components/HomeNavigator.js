import { View, Platform } from 'react-native';
import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import { Home } from '../screens/Home';

export const HomeNavigator = createDrawerNavigator(
  {
    Home: Home,
  },
  {
    initialRouteName: 'Home'
  }
)
