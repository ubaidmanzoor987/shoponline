import React, { Component } from 'react';
import { StyleSheet, Text, Button, View, TextInput, TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import { Logo } from '../components/LogoComponent';
import  LoginForm from '../components/LoginForm';


export class Login extends Component {

  constructor(props)
  {
    super(props);
    console.log(props);
  }

  static navigationOptions = {
    title: 'Login',
    headerStyle: {
      backgroundColor:'rgba(52, 87, 85,1)',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    }
  }

  render() {
    return(
      <KeyboardAvoidingView  style={styles.container} behavior="padding" enabled>
        <Logo />
        <LoginForm
        signUpPress={() => this.props.navigation.navigate('SignUp')}
        goToHomeScreen={() => this.props.navigation.navigate('HomeNavigator')}
        forgetPasswordPress={() => this.props.navigation.navigate('ForgotPassword')} />
      </KeyboardAvoidingView >
    )
  }
}



const styles = StyleSheet.create({
    container: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'gray',
    }
});
